module.exports = {
  platform: "gitlab",
  token: process.env.RENOVATE_TOKEN,
  enabledManagers: [
    "terraform",
  ],
  repositories: ["TheKangaroo/renovate-opentofu"]
};


